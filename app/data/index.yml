section1:
  paragraphs1: |
    <span>S</span>hopping today takes no more than a click of a button.
    No dressing rooms, no lines at the cash register, no clingy salespeople or overwhelming aisles of goods.
    Instead, consumers can save gift ideas in digital shopping carts, save online profiles for even
    faster checkouts and easily compare product reviews and prices across different sites.
  paragraphs2: |
    The convenience and ease of online shopping is expected to make online holiday shopping hit new highs
    in 2016 as consumers skip the crowds and capitalize on sales from the comfort of their couches.
  paragraphs3: |
    According to Adobe Digital Insights, sales are expected to total $91.
    6 billion over the 57 days of the 2016 winter holiday shopping season,
    an increase of 11 percent over the previous year. For the first time,
    shoppers are expected to visit retailers’ mobile sites more than desktop
    sites as they browse for gift ideas at the time and place that suits them best.
  paragraphs4: |
    But whether shoppers are buying online or in physical stores,
    the goal for retailers is to make the shopping experience as
    frictionless as possible. To that end, retailers have been testing various
    technologies to keep shoppers coming back to their web sites and brick-and-mortar locations.
  paragraphs5: |
    For instance, imagine when you walk into a store, a robot ushers you through
    aisles to the items you want to buy. A smart mirror in the dressing room suggests
    the perfect boots for the jeans you tried on. And by using virtual reality goggles,
    you’re able to see how a couch looks in your home — without leaving the store.
  paragraphs6: |
    	Retailers’ goal is twofold: They want to chip away at the things that can make shopping
      a hassle — like long lines or dull dressing rooms — while adding more convenience and
      personalized service.
  paragraphs7: |
    “If it’s high-touch retail, you want to provide great experiences and entertainment,”
    said Michael Klein, Director of Industry Strategy at tech company Adobe. “But if it’s
    grocers or big-box stores, the technology needs to make that experience more seamless and efficient.”

  title1: MAKING SMART STORES
  paragraphs8: |
    Retailers are using robots and other smart technologies to improve the in-store experience.

  paragraphs9: |
    Some of these technologies are aimed at saving customers time, something that is a top
    priority for shoppers. In fact, according to a recent Forrester retail survey,
    shoppers ranked “saving time” as the top reason to use in-store technology.
  paragraphs10: |
    One technology being tested is radio frequency identification (RFID) to
    embed data in clothing tags that allows items to be tracked throughout the stores.
    Zappos tested RFID last when it installed a Magic Checkout at an event in San Francisco.
    Shoppers walked over an RFID-reading mat, which scanned the RFID-tagged merchandise in less
    than a second. Through mobile payment, shoppers could complete the sale in seconds.

  paragraphs11: |
    There are other uses for RFID, too. UGGs, for instance, is tagging its shoes in
    one store with RFID so that when shoppers step on a carpet,
    style tips and product information like "what sizes are in stock" pop up on a screen.


  image1_1:
    aspect: wide
    src: images/shopping-bags-desktop.png
  image1_2:
    aspect: wide
    src: images/shopping-bags-tablet.png
  image1_3:
    aspect: wide
    src: images/shopping-bags-mobile.png
  paragraphs12: |
    Retailers also are adding smart technologies to the oft-forgotten dressing room.
    Brands that include Bloomingdale’s, Neiman Marcus and Nordstrom are testing smart
    mirrors that do things like suggest shoes and other accessories. Shoppers also can
    see how they look in an outfit without trying it on.

  paragraphs13: |
    The dressing room certainly is ripe for change. Seventy-one percent of shoppers who
    try on clothes in the fitting room become buyers, but only about a third step foot in
    dressing rooms to begin with, said Paco Underhill, a retail consultant.

  paragraphs14: |
    “Dressing rooms are one of the few things that are different between online and in-store,”
    said Andrew Hogan, a Forrester analyst. “You have the opportunity to make something that was
    a dead and isolated experience into something much more useful and exciting.”

  title2: MAY I HELP YOU?

  paragraphs15: |
    Merchants also are looking for ways to improve customer service —
    both in stores and online — by experimenting with robots with human characteristics.

  paragraphs16: |
    While not common in stores yet, robots have huge potential for retailers: Technology
    experts predict that robotics and artificial intelligence will be in many homes by 2025 —
    the same time that experts expect them to become common on retail floors.

  paragraphs17: |
    Orchard Supply Hardware, which is owned by Lowe’s, is the first to
    use an autonomous robot on the retail floor.

  paragraphs18: |
    The robot, called “OSHbot,” speaks multiple languages and is equipped
    with image recognition to help identify what an item is and guide customers to
    products in the store.
  paragraphs19: |
    OSHbot has been successful so far. In the first six months, OSHbot helped 3,000
    people with 3,500 searches — about what a human store associate would achieve in the
    same time period.

  paragraphs20: |
    Retailers also are testing robots online through what’s called “conversational commerce.”
    Chatbots simulate human conversation in the same way that Amazon Echo acts as a digital assistant.
    Most chatbots run in messaging apps like Facebook Messenger.

  paragraphs21: |
    The Sephora beauty product store chain and the H&M clothing chain have launched bots
    on the Kik messaging app that can do things like pull together outfits based on preferences
    and commands a shopper types like “leather jacket.” The chatbots are programmed to speak in
    casual language to simulate a conversation you might have with a friend.

  paragraphs22: |
    “It’s the ability to have a conversation with a device that removes the friction that may
    occur when we’re required to deal with humans or type in a whole bunch of information in a
    search box,” Adobe’s Klein said.

section2:
  paragraphsIntro: |
    Technology experts predict
    that robotics <br>and artificial intelligence will make the leap
    <br>to daily life by 2025.
  paragraphs0: |
    Orchard Supply Hardware, which is owned the by Lowe’s, is the first to use an autonomous robot on the retail floor.
  paragraphs1: |
    The robot, called “OSHbot,” speaks multiple languages and is equipped with image recognition to help
    identify what an item is and guide customers to products in the store.
  paragraphs2: |
    OSHbot has been successful so far. In the first six months, OSHbot helped 3,000 people with 3,500
    searches — about what a human store associate would achieve in the same time period.
  paragraphs3: |
    Retailers also are testing robots online through what’s called “conversational commerce.” Chatbots
    simulate human conversation in the same way that Amazon Echo acts as a digital assistant. Most chatbots
    run in messaging apps like Facebook Messenger.
  paragraphs4: |
    The Sephora beauty product store chain and the H&M clothing chain have launched bots on the Kik
    messaging app that can do things like pull together outfits based on preferences and commands a
    shopper types, such as “leather jacket.”
  paragraphs5: |
    “It’s the ability to have a conversation with a device that removes the friction that may occur when
    we’re required to deal with humans or type in a whole bunch of information in a search box,” Adobe’s Klein said.
  image1_1:
    src: images/cat-couch-desktop.gif
  image1_2:
    src: images/cat-couch-tablet.gif
  image1_3:
    src: images/cat-couch-mobile.gif
  title1: Virtual Shopping
  paragraphs6: |
    Retailers also are looking to enhance the shopping experience through virtual and augmented reality.

  paragraphs7: |
    VR allows consumers to see what products in their homes would look like without leaving the store.

  paragraphs8: |
    These are areas that retailers want to be in going forward. The VR retail market is expected to rise to $1.6 billion by 2025, according to a Goldman Sachs report. And AR could reach $30 billion in sales, as projected by a Digi Capital report.

  image2:
    aspect: square
    type: float-left
    src: images/vr-desktop.jpg
  paragraphs9: |
    Some chains have experimented with AR. Dippin’ Dots, for instance, enabled customers in its loyalty program to make it rain Dippin’ Dots in its stores through AR. And Walgreens added 3-D imagery to its in-store maps.

  paragraphs10: |
    More big-name retailers are looking at VR, including Lowe’s. The chain last year created the Holoroom, which allows customers to design their kitchen or bathroom space while in its stores, using an app. Then, by using virtual reality goggles, shoppers could step into the design.

  paragraphs11: |
    Swedish furniture chain IKEA also launched a VR experiment this year, enabling shoppers to explore a kitchen where they can open drawers and change cabinet colors.


  paragraphs12: |
    Experts say shoppers will continue to see more retailers testing these types of technologies. “Things are moving fast,” said Artemis Berry, vice president of digital retail for the National Retail Federation, a trade group.


  paragraphs13: |
    And that means, 20 from years from now, shoppers may feel like they’re living in the popular 1960s cartoon “The Jetsons,” with robots that cater to your every need, both in the store and online.
footer:
  text:|
    changing the world through digital experiences
