require.config({
    paths: {
        TweenLite: 'https://cdnjs.cloudflare.com/ajax/libs/gsap/1.18.0/TweenLite.min',
        TimelineLite: 'https://cdnjs.cloudflare.com/ajax/libs/gsap/1.18.0/TimelineLite.min',
        TweenMax: 'https://cdnjs.cloudflare.com/ajax/libs/gsap/1.18.0/TweenMax.min',
        TimelineMax: 'https://cdnjs.cloudflare.com/ajax/libs/gsap/1.18.0/TimelineMax.min',
        EasePack: 'https://cdnjs.cloudflare.com/ajax/libs/gsap/1.18.0/easing/EasePack.min',
        BezierPlugin: 'https://cdnjs.cloudflare.com/ajax/libs/gsap/1.18.0/plugins/BezierPlugin.min',
        ScrollMagic: 'https://cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.5/ScrollMagic.min',
        ScrollMagicAddIndicators: 'https://cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.5/plugins/debug.addIndicators.min',
        ScrollTo:'https://cdnjs.cloudflare.com/ajax/libs/gsap/1.19.0/plugins/ScrollToPlugin.min',
        ScrollMagicGSAP: 'https://cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.5/plugins/animation.gsap.min',
        'slick-carousel': 'https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.5.8/slick.min',
        JKSizeClassEventHandler: 'adobe_retail-tech/bower_components/JKSizeClassEventHandler/js/JKSizeClassEventHandler.min',
        GrowthInfographic: PaidPost.assets + "js/lib/GrowthInfographic"
    },
    packages: [

    ],
    shim: {

    }
});
