// Begin Dependencies
var core = {};
core.gS = {};
core.sM = {};
var $ = require("jquery");

require("slick-carousel");
// GSAP
var TweenMax = require("TweenMax");
var TimelineMax = require("TimelineMax");
// ScrollMagic
var ScrollMagic = require("ScrollMagic");
require("ScrollMagicAddIndicators"); // manually comment out this line for production builds
require("ScrollMagicGSAP");
require("ScrollTo");
var JKSizeClassEventHandler = require("JKSizeClassEventHandler");
core.controller = new ScrollMagic.Controller();
// End Dependencies

// Write Application Code Here (or in any other js/src/*.js file)

$(function() {
    size();
    greenSock();
    scrollMagicFn();
    listenners();
    cat();
});

function cat() {
    $(".cat").each(function() {
        var gifSource = $(this).find("img").attr('src');
        $(this).find("img").attr('src', "");
        $(this).find("img").attr('src', gifSource + "?" + new Date().getTime());
    });

}

function size() {
    core.masthead = $("#masthead").height();
    core.paidTopBar = $("#paid-top-bar-container").height();
    core.headerHeight = core.masthead + core.paidTopBar;
}
function listenners() {
    $(document).on("click", "a[href^='#']", function(e) {
        var id = $(this).attr("href");
        if ($(id).length > 0) {
            e.preventDefault();

            // trigger scroll
            core.controller.scrollTo(id);

            // if supported by the browser we can even update the URL.
            if (window.history && window.history.pushState) {
                history.pushState("", document.title, id);
            }
        }
    });
}

function greenSock() {
    core.gS.header = new TimelineMax();
    core.gS.header.to('#header-text-primary', 1, {autoAlpha: 1},0.5);
}

function scrollMagicFn() { 
    $(window).resize(function() {
        size();
    });

    // core.sM.header = new ScrollMagic.Scene({triggerElement: "#main-header-container", triggerHook: 0.11, duration: "5%"}).setTween(core.gS.header).reverse(false).addTo(core.controller);

    core.controller.scrollTo(function(newpos) {
        TweenMax.to(window, 1, {
            scrollTo: {
                y: newpos,
                offsetY: core.headerHeight
            }
        });
    });
}
