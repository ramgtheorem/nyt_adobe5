module.exports.register = function (Handlebars, options) {
	// Based on http://stackoverflow.com/a/10233247
	Handlebars.registerHelper('json', function (context) {
		return JSON.stringify(context);
	});
};